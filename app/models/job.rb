class Job < ActiveRecord::Base
  has_many :tasks

  def checkcomplete
    self.tasks.each do |t|
      if t.done? false
        set t.job.done false
        break
      else
        set t.job.done true
      end
    end

  end
end