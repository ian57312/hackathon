json.array!(@jobs) do |job|
  json.extract! job, :id, :name, :duedate, :location, :done
  json.url job_url(job, format: :json)
end
