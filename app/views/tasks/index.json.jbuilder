json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :description, :job_id, :done
  json.url task_url(task, format: :json)
end
